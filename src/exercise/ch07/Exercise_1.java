package exercise.ch07;

/**
 * 1. Draw a table that shows the value for i and n during the execution of the loop
 *
 *      +-----------+-----+-----+
 *      | ITERATION | i   | n   |
 *      +-----------+-----+-----+
 *      | 1         | 10  | 10  |
 *      | 2         |  5  | 10  |
 *      | 3         |  6  | 10  |
 *      | 4         |  3  | 10  |
 *      | 5         |  4  | 10  |
 *      | 6         |  2  | 10  |
 *      +-----------+-----+-----+
 *
 * 2. What is the output of this program?
 *      10
 *      5
 *      6
 *      3
 *      4
 *      2
 *
 * 3. Can you prove that this loop terminates for any positive value of n?
 *      Yes. When a number is odd, if you add 1, the number becomes even. Then
 *      dividing it by two always leaves a result smaller than the initial
 *      value except if it was 1. But if it's 1, the loop terminates.
 *
 */

public class Exercise_1 {

    public static void main(String[] args) {
        loop(10);
    }

    public static void loop(int n){
        int i = n;
        while (i > 1){
            System.out.println(i);
            if (i % 2 == 0){
                i = i / 2;
            } else {
                i = i + 1;
            }
        }
    }

}
