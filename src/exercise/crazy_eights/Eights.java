package exercise.crazy_eights;

import java.util.Scanner;

public class Eights {

    private Player one;
    private Player two;
    private Hand drawPile;
    private Hand discadPile;
    private Scanner in;

    public Eights(){
        Deck deck = new Deck("Deck");
        deck.shuffle();

        int handSize = 5;
        one = new Player("Sjon");
        deck.deal(one.getHand(), handSize);

        two = new Player("Matthijs");
        deck.deal(two.getHand(), handSize);

        discadPile = new Hand("Discards");
        deck.deal(discadPile, 1);

        drawPile = new Hand("Draw pile");
        deck.dealAll(drawPile);

        in = new Scanner(System.in);
    }

    public void displayState(){
        one.displey();
        two.displey();
        discadPile.display();
        System.out.println("Draw pile:");
        System.out.println(drawPile.size() + " cards");
    }

    public void waitForUser(){
        in.nextLine();
    }

    public void takeTurn(Player player){
        Card prev = discadPile.last();
        Card next = player.play(this, prev);
        discadPile.addCard(next);

        System.out.println(player.getName() + " plays " + next);
        System.out.println();
    }

    public void playGame(){
        Player player = one;

        while(!isDone()){
            displayState();
            waitForUser();
            takeTurn(player);
            player = nextPlayer(player);
        }

        System.out.println(one.score());
        System.out.println(two.score());
    }

    public Card draw(){
        if(drawPile.empty()){
            reshuffle();
        }
        return drawPile.popCard();
    }

    public Player nextPlayer(Player current){
        return current == one ? two : one;
    }

    public boolean isDone(){
        return one.getHand().empty() || two.getHand().empty();
    }

    public void reshuffle(){
        Card prev = discadPile.popCard();
        discadPile.dealAll(drawPile);
        discadPile.addCard(prev);
        drawPile.shuffle();
    }

}
