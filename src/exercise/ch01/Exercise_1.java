package exercise.ch01;

public class Exercise_1 {

    /**
     * 1. In computer jargon, what is the difference between a statement and a comment?
     *      A statement is a piece of code that can be executed and a comment is a piece of text inside your code to help the reader understand the code.
     *
     * 2. What does it mean to say that a program is portable?
     *      That the code can be taken to another device and be executed without any modifications
     *
     * 3. In common english, what does compile mean?
     *      To collect information
     *
     * 4. What is an executable? Why is that word used as a noun?
     *      An executable is a file that can perform certain computations. It is a noun because a file can either be executed or it can not.
     */

}
