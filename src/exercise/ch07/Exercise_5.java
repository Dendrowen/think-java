package exercise.ch07;

public class Exercise_5 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        for(double i = 0.1; i <= 100; i *= 10){
            check(i);
        }
        System.out.println();
        for(double i = -0.1; i >= -100; i *= 10){
            check(i);
        }
    }

    /**
     * Step 4. Prints the exponent of Euler's number using different methods.
     * @param x The exponent
     */
    public static void check(double x){
        System.out.printf("%4.0f\t%16.8f\t%16.8f\n", x, myexpImproved(x, 40), Math.exp(x));
    }

    /**
     * Step 1.
     * @param x The exponent
     * @param n The accuracy
     * @return Returns the exponent of euler's number
     */
    public static double myexp(double x, int n){
        double result = 1;
        for(int i = 1; i <= n; i++){
            double p = Math.pow(x, i);
            double f = Exercise_4.factorial(i);
            result += p / f;
        }
        return result;
    }

    /**
     * Step 2.
     * @param x The exponent
     * @param n The accuracy
     * @return Returns the exponent of euler's number
     */
    public static double myexpImproved(double x, int n){
        double result = 1;
        double p = 1;
        double f = 1;
        for (int i = 1; i <= n; i++){
            p = p * x;
            f = f * i;
            result += p / f;
        }
        return result;
    }

}
