package exercise.ch09;

import java.util.Scanner;

public class Exercise_2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("give me text: ");
        String t = in.nextLine();
        printHist(letterHist(t));
    }

    public static void printHist(int[] hist){
        for(int i = 0; i < hist.length; i++){ //print row with letters a - z
            System.out.print((char) (i + 97) + "\t");
        }
        System.out.println();
        for(int i = 0; i < hist.length; i++){ //print numbers in histogram
            System.out.print(hist[i] + "\t");
        }
    }

    public static int[] letterHist(String text){
        int[] hist = new int[26];
        text = text.toLowerCase();

        for(char c : text.toCharArray()){
            // a == 97 in ascii
            // a - 97 = 0
            // b - 97 = 1
            // .
            // z - 97 = 25
            if(c < 'a' || c > 'z') continue;
            hist[((int) c) - 97]++;
        }

        return hist;
    }

}
