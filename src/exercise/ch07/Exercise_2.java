package exercise.ch07;

public class Exercise_2 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(squareRoot(81));
    }

    /**
     * Calculates the approximate root of the given number
     * @param x The number to calculate the root off
     * @return The approximate root of x
     */
    public static double squareRoot(double x){
        double diff = 1;
        double x0 = x / 2;
        do{
            double x1 = (x0 + x/x0) / 2;
            diff = Math.abs(x1 - x0);
            x0 = x1;
        } while (diff > 0.00001);
        return x0;
    }

}
