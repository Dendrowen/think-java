package exercise.ch02;

public class Exercise_2 {

    /**
     * main method.
     * @param args
     */
    public static void main(String[] args) {
        String day = "Monday";
        int date = 4;
        String month = "March";
        int year = 2019;

        System.out.println("American format:");
        System.out.print(day);
        System.out.print(", ");
        System.out.print(month);
        System.out.print(" ");
        System.out.print(date);
        System.out.print(", ");
        System.out.println(year);

        System.out.println("European format:");
        System.out.print(day);
        System.out.print(" ");
        System.out.print(date);
        System.out.print(" ");
        System.out.print(month);
        System.out.print(" ");
        System.out.println(year);
    }

}
