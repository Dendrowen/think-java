package exercise.ch04;


/**
 * 1. This code executes the following text:
 *      No, I wug.
 *      You wugga wug.
 *      I wug.
 *
 * 2. Draw a stack diagram that shows the state of the program the first time ping() is invoked
 *      +---------------------------------+
 *      |         +-------------+         |
 *      | main    | args [null] |         |
 *      |         +-------------+         |
 *      +---------------------------------+
 *
 *      +---------------------------------+
 *      |                                 |
 *      | zoop                            |
 *      |                                 |
 *      +---------------------------------+
 *
 *      +---------------------------------+
 *      |                                 |
 *      | baffle                          |
 *      |                                 |
 *      +---------------------------------+
 *
 *      +---------------------------------+
 *      |                                 |
 *      | ping                            |
 *      |                                 |
 *      +---------------------------------+
 *
 * 3. What happens if uou invoke baffle() at the end of the ping() method?
 *      It creates an infinite loop between two methods until a StackOverflowError occurs
 *
 */

public class Exercise_1 {

    /**
     * Executes baffle then prints "You wugga" and executes baffle again.
     */
    public static void zoop(){
        baffle();
        System.out.print("You wugga ");
        baffle();
    }

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.print("No, I ");
        zoop();
        System.out.print("I ");
        baffle();
    }

    /**
     * Prints "wug" and executes ping
     */
    public static void baffle(){
        System.out.print("wug");
        ping();
    }

    /**
     * Prints a period
     */
    public static void ping(){
        System.out.println(".");
    }

}
