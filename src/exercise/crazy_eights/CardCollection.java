package exercise.crazy_eights;

import java.util.ArrayList;
import java.util.Random;

public class CardCollection {

    private String label;
    ArrayList<Card> cards;

    public CardCollection(String label){
        this.label = label;
        this.cards = new ArrayList<Card>();
    }

    public void deal(CardCollection that, int n){
        for(int i = 0; i < n; i++){
            that.addCard(popCard());
        }
    }

    public void dealAll(CardCollection that){
        deal(that, size());
    }

    public void swapCards(int a, int b){
        Card t = cards.get(a);
        cards.set(a, cards.get(b));
        cards.set(b, t);
    }

    public void shuffle(){
        Random r = new Random();
        for(int i = size() - 1; i > 0; i--){
            swapCards(i, r.nextInt(i));
        }
    }

    public String getLabel() {
        return label;
    }

    public Card getCard(int i){
        return cards.get(i);
    }

    public Card last(){
        return getCard(size() - 1);
    }

    public void addCard(Card card){
        cards.add(card);
    }

    public Card popCard(){
        return popCard(size() - 1);
    }

    public Card popCard(int i){
        return cards.remove(i);
    }

    public int size(){
        return cards.size();
    }

    public boolean empty(){
        return size() == 0;
    }

}
