package exercise.ch03;

import java.util.Scanner;

public class Exercise_3 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Number of seconds: ");
        int seconds = in.nextInt();

        int hour = seconds / (60 * 60);     // This is a great example of integer division! (5 / 2 == 2)
        int minute = (seconds / 60) % 60;   // First we do an integer division, then cut of the hours
        int second = seconds % 60;          // Simply a mod 60

        System.out.printf("%d seconds = %d hours, %d minutes and %d seconds", seconds, hour, minute, second);

    }

}
