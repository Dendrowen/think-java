package exercise.ch06;

public class Exercise_3 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(isTriangle(3,4,5));  // true
        System.out.println(isTriangle(3,4,8));  // false
        System.out.println(isTriangle(4,4,8));  // false
        System.out.println(isTriangle(10,4,8)); // true
    }

    /**
     * Determines if a triangle can be made with the given lengths.
     * @param a side a
     * @param b side b
     * @param c side c
     * @return True if a triangle can be made. False otherwise.
     */
    public static boolean isTriangle(int a, int b, int c){
        return a + b > c && b + c > a && c + a > b;
    }

}
