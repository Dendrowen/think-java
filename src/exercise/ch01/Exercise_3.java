package exercise.ch01;

/**
 * You can use this example by uncommenting 'main' method at the time
 */
public class Exercise_3 {


    // working
//    public static void main(String[] args) {
//        System.out.println("Hello World!");
//    }

    // 1. Remove one of the open squiggly braces
//    public static void main(String[] args)
//        System.out.println("Hello World!");
//    }


    // 2. Remove one of the close squigly braces
//    public static void main(String[] args) {
//        System.out.println("Hello World!");
//

    // 3. Instead of main, write mian
//    public static void mian(String[] args) {
//        System.out.println("Hello World!");
//    }

    // 4. Remove the word static
//    public void main(String[] args) {
//        System.out.println("Hello World!");
//    }

    // 5. Remove the word public
//    static void main(String[] args) {
//        System.out.println("Hello World!");
//    }

    // 6. Remove the word System
//    public static void main(String[] args) {
//        .out.println("Hello World!");
//    }

    // 7. Replace println with Println
//    public static void main(String[] args) {
//        System.out.Println("Hello World!");
//    }

    // 8. Replace println with print
//    public static void main(String[] args) {
//        System.out.print("Hello World!");
//    }

    // 9. Delete one of the parentheses
//    public static void main(String[] args) {
//        System.out.println("Hello World!";
//    }

    // 9. Add an extra parenthesis
//    public static void main(String[] args)) {
//        System.out.println("Hello World!");
//    }
}
