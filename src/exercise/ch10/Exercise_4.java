package exercise.ch10;

import java.math.BigInteger;

public class Exercise_4 {
    public static void main(String[] args) {

        for(int i = 0; i <= 1000; i++) {
            System.out.printf("factorial of %d is %d\n", i, factorial(i));
        }

    }

    public static BigInteger factorial(int n) {
        if (n == 0) {
            return BigInteger.valueOf(1);
        }
        BigInteger recurse = factorial(n - 1);
        return recurse.multiply(BigInteger.valueOf(n));
    }
}