package exercise.ch08;

/**
 * Draw a stack diagram just before mus returns
 *      +-----------------------------------+
 *      |         +--------------+          |
 *      | main    | args [null]  |          |
 *      |         +--------------+          |
 *      +-----------------------------------+
 *
 *      +-----------------------------------+
 *      |         +----------------------+  |
 *      | mus     | zoo {2, 4, 6, 8, 10} |  |
 *      |         +----------------------+  |
 *      +-----------------------------------+
 */
public class Exercise_3 {

    /**
     * Creates an array with values (index + 1)
     * @param n The size of the array
     * @return The created array of size n
     */
    public static int[] make(int n){
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = i + 1;
        }
        return a;
    }

    /**
     * Doubles each value in the provided array
     * @param jub The array to double
     */
    public static void dub(int[] jub){
        for(int i = 0; i < jub.length; i++){
            jub[i] *= 2;
        }
    }

    /**
     * Calculates the sum of all values in the provided array
     * @param zoo The array
     * @return The sum of al values in array 'zoo'
     */
    public static int mus(int[] zoo){
        int fus = 0;
        for(int i = 0; i < zoo.length; i++){
            fus += zoo[i];
        }
        return fus;
    }

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        int[] bob = make(5);
        dub(bob);
        System.out.println(mus(bob));
    }

}
