package exercise.ch11;

public class Exercise_1 {

    /**
     * Documentation URL: https://docs.oracle.com/javase/7/docs/api/java/awt/Rectangle.html
     *
     * Pure methods:
     *      contains()
     *      createIntersection()
     *      createUnion()
     *      equals()
     *      getBounds()
     *      getBounds2D()
     *      getHeight()
     *      getLocation()
     *      getSize()
     *      getWidth()
     *      getX()
     *      getY()
     *      intersection()
     *      intersects()
     *      isEmpty()
     *      outcode()
     *      toString()
     *      union()
     *
     * Modifier methods:
     *      add()
     *      grow()
     *      setBounds()
     *      setLocation()
     *      setRect()
     *      setSize()
     *      translate()
     *
     */

}
