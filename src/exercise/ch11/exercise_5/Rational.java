package exercise.ch11.exercise_5;

public class Rational {

    private int numerator;
    private int denominator;

    /**
     * Empty constructor
     */
    public Rational(){
        this(0, 1);
    }

    /**
     * Constructor. Creates a fraction based on the given value
     * @param numerator The upper value
     * @param denominator The lower value
     */
    public Rational(int numerator, int denominator){
        this.numerator = numerator;
        this.denominator = denominator;
    }

    /**
     * Negates the value of this fraction
     */
    public void negate(){
        this.numerator = -this.numerator;
    }

    /**
     * Switches the numerator and the denominator
     */
    public void invert(){
        int temp = numerator;
        numerator = denominator;
        denominator = temp;
    }

    /**
     * Finds the greatest common divider and reduces the fraction
     * to the smallest possible integer numbers
     * @return A new Rational with the reduced values
     */
    public Rational reduce(){
        int gcd = gcd(this.numerator, this.denominator);
        return new Rational(this.numerator / gcd, this.denominator / gcd);
    }

    /**
     * Finds the greatest common divider between the two given values
     * @param n
     * @param m
     * @return The greatest common divider
     */
    private int gcd(int n, int m){
        if(m == 0){
            return n;
        } else {
            return gcd(m, n % m);
        }
    }

    /**
     * Adds two Rationals together and returns the reduced sum.
     * @param that The other Rational to add to this one
     * @return A new reduced Rational
     */
    public Rational add(Rational that){
        Rational r1 = new Rational(
                this.numerator * that.denominator,
                this.denominator * that.denominator
        );
        Rational r2 = new Rational(
                that.numerator * this.denominator,
                that.denominator * this.denominator
        );
        Rational result = new Rational(
                r1.numerator + r2.numerator,
                r1.denominator
        );
        return result.reduce();
    }

    /**
     * Calculates the floating-point value
     * @return the value of numerator divided by denominator
     */
    public double toDouble(){
        return ((double) this.numerator) / this.denominator;
    }

    /**
     * Returns a string representation of the Rational
     * @return
     */
    public String toString(){
        return this.numerator + "/" + this.denominator;
    }

}
