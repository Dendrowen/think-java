package exercise.ch08;

import java.util.Arrays;
import java.util.Random;

public class Exercise_1 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        double[] arr = new double[10000];
        for(int i = 0; i < arr.length; i++){
            arr[i] = Math.random() * 10;
        }
        arr = powArray(arr, 2);
        int[] hist = histogram(arr, 100);
        System.out.println(Arrays.toString(hist));
    }

    /**
     * Raises an entire array to a certain power
     * @param arr The array to power
     * @param power The exponent
     * @return The same array with every element raised
     */
    public static double[] powArray(double[] arr, int power){
        for(int i = 0; i < arr.length; i++){
            arr[i] = (int) Math.pow(arr[i], power);
        }
        return arr;
    }

    /**
     * Generates a histogram based on the served array
     * @param values An array of values to count
     * @param range The highest value in the provided array
     * @return An array with the counts
     */
    public static int[] histogram(double[] values, int range){
        int[] hist = new int[range];
        for(int i = 0; i < values.length; i++){
            hist[(int) values[i]]++;
        }
        return hist;
    }

}
