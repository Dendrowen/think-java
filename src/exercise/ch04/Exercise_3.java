package exercise.ch04;

public class Exercise_3 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        printAmerican("Saturday", 22, "July", 2015);
        printAmerican("Thursday", 28, "February", 2019);

        System.out.println();

        printEuropean("Saturday", 22, "July", 2015);
        printEuropean("Thursday", 28, "February", 2019);
    }

    /**
     * Prints out the passed date in an American format
     * @param day   The day of the week.    e.g. Monday
     * @param date  The day of the month.   e.g. 21
     * @param month The month in text.      e.g. June
     * @param year  The year in 4 digits    e.g. 2019
     */
    public static void printAmerican(String day, int date, String month, int year){
        System.out.print(day);
        System.out.print(", ");
        System.out.print(month);
        System.out.print(" ");
        System.out.print(date);
        System.out.print(", ");
        System.out.println(year);
    }

    /**
     * Prints out the passed date in an Europian format
     * @param day   The day of the week.    e.g. Monday
     * @param date  The day of the month.   e.g. 21
     * @param month The month in text.      e.g. June
     * @param year  The year in 4 digits    e.g. 2019
     */
    public static void printEuropean(String day, int date, String month, int year){
        System.out.print(day);
        System.out.print(" ");
        System.out.print(date);
        System.out.print(" ");
        System.out.print(month);
        System.out.print(" ");
        System.out.println(year);
    }

}
