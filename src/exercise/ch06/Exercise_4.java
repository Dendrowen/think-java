package exercise.ch06;

public class Exercise_4 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        // step 3
        System.out.println(multAdd(1.0, 2.0, 3.0));

        // step 4
        System.out.println(multAdd(0.5, Math.cos(Math.PI / 4), Math.sin(Math.PI / 4)));
        System.out.println(Math.log(10) + multAdd(2, Math.log(2), Math.log(5)));

        // step 5
        System.out.println(expSum(4));
    }

    /**
     * Calculates the product of a and b and adds c
     * @param a
     * @param b
     * @param c
     * @return the product of a and b plus c
     */
    public static double multAdd(double a, double b, double c){
        return a * b + c;
    }

    public static double expSum(double x){
        return (multAdd(x, Math.exp(-x), Math.sqrt(1 - Math.exp(-x))));
    }

}
