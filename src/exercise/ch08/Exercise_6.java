package exercise.ch08;

public class Exercise_6 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        int[] numbers = {2, 5};
        System.out.println(areFactors(12, numbers));
        System.out.println(areFactors(10, numbers));
    }

    /**
     * Checks if all values in tha array are factors of n
     * @param n The base
     * @param arr The factors
     * @return true if all are factors. Otherwise false
     */
    public static boolean areFactors(int n, int[] arr){
        for(int a : arr) {
            if (n % a != 0) {
                return false;
            }
        }
        return true;
    }

}
