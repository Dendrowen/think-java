package exercise.ch07;

public class Exercise_6 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.printf("%.10f\n", gauss(-4, 35));
        System.out.printf("%.10f\n", Math.exp(-4));
    }

    /**
     * Negative exponent of Euler's number
     * @param x The exponent
     * @param n The accuracy
     * @return The exponent x of Euler's number
     */
    public static double gauss(double x, int n){
        double result = 1;
        double p = 1;
        double p2 = 1;
        double f = 1;
        for(int i = 1; i < n; i++){
            p = p * -1;
            p2 = p2 * x * x;
            f = f * i;
            result += p * p2 / f;
        }
        return result;
    }

}
