package exercise.ch03;

public class Exercise_1 {

    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        System.out.printf("This is secretly a integer digit: %f", 12);                      //IllegalFormatConversionException
        System.out.printf("This is secretly a floating point value: %d", 4.6);              //IllegalFormatConversionException
        System.out.printf("The first value (%d) is there but the second is not (%d)", 4);   //MissingFormatArgumentException
    }

}
