package exercise.ch12.test;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Dice[] yatzee = new Dice[5];
        for(int i = 0; i < yatzee.length; i++){
            yatzee[i] = new Dice(6);
        }
        int count = 0;
        while(!isYatzee(yatzee)){
            printDices(yatzee);
            for(int i = 0; i < yatzee.length; i++){
                yatzee[i].roll();
            }
            count++;
        }

        System.out.printf("YATZEE! in %d iterations\n", count);
        printDices(yatzee);



    }

    public static boolean isYatzee(Dice[] dices){
        int number = dices[0].getNumberOnTop();
        int count = 1;
        for(int i = 1; i < dices.length; i++){
            if(dices[i].getNumberOnTop() == number){
                count++;
            }
        }
        return count >= 5;
    }

    public static void printDices(Dice[] dices){
        System.out.println(Arrays.toString(dices));
    }

}
