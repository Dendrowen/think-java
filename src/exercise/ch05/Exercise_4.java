package exercise.ch05;

public class Exercise_4 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        checkFermat(4 ,6, 7, 3);
    }

    /**
     * Checks if the Fermat's theorem checks out
     * @param a First number
     * @param b Second number
     * @param c Third number
     * @param n Exponent
     */
    public static void checkFermat(int a, int b, int c, int n){
        double abSquared = Math.pow(a, n) + Math.pow(b, n);
        double cSquared = Math.pow(c, n);

        System.out.printf("a x b squared: %.0f\n", abSquared);
        System.out.printf("c Squared:     %.0f\n", cSquared);

        if(n > 2 && abSquared == cSquared){
            System.out.println("Holy smokes, Fermat was wrong!");
        } else {
            System.out.println("No, that doesn't work");
        }

    }

}
