package exercise.ch06;

public class Exercise_2 {

    public static void main(String[] args) {
        System.out.println(isDivisible( 5,  2)); // false
        System.out.println(isDivisible(20,  4)); // true
        System.out.println(isDivisible(312, 4)); // true
        System.out.println(isDivisible( 2,  6)); // false

    }

    public static boolean isDivisible(int n, int m){
        return n % m == 0;
    }

}
