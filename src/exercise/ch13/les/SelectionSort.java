package exercise.ch13.les;

import java.util.Arrays;
import java.util.Random;

public class SelectionSort {

    public static void main(String[] args) {
        int[] array = rendomArray(10, 30);
        System.out.println(Arrays.toString(array));
        // Sort your array here
        System.out.println(Arrays.toString(array));
    }

    public static int[] rendomArray(int size, int max){
        int[] n = new int[size];
        Random r = new Random();
        for(int i = 0; i < size; i++){
            n[i] = r.nextInt(max);
        }
        return n;
    }

    public static int indexLowest(int[] array, int low, int high){
        int l = low;
        while(low++ < high){
            if(array[low] < array[l]) l = low;
        }
        return l;
    }

    public static void swap(int[] array, int low, int high){
        int temp = array[low];
        array[low] = array[high];
        array[high] = temp;
    }

}
