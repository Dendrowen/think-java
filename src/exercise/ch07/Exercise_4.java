package exercise.ch07;

import java.math.BigInteger;

public class Exercise_4 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(factorial(4));
        System.out.println(factorial(10));
    }

    /**
     * Calculates the product of the given number and all smaller numbers
     * @param n number
     * @return The product of n and all values below it
     */
    public static int factorial(int n){
        int result = n;
        while(n > 1){
            n--;
            result = result * n;
        }
        return result;
    }

}
