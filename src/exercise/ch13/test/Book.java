package exercise.ch13.test;

public class Book {

    private final String isbn;
    private final String title;
    private boolean inHouse;

    public Book(String isbn, String title){
        this.isbn = isbn;
        this.title = title;
        this.inHouse = true;
    }

    public void loanToPerson(){
        if(isInHouse()) {
            System.out.println(getTitle() + " is back");
            this.inHouse = false;
        } else {
            System.out.println("ERROR: this book is already out!");
        }
    }

    public void returned(){
        if(isInHouse()){
            System.out.println("ERROR: this book is already in!");
        } else {
            System.out.println(getTitle() + " is loaned");
            this.inHouse = true;
        }
    }

    public String getIsbn() { return isbn; }
    public String getTitle() { return title; }
    public boolean isInHouse() { return inHouse; }

}
