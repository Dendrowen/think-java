package exercise.ch13.test;

public class Main {

    public static void main(String[] args) {
        Library lib = new Library("The happy cover", 5);

        lib.addBook(new Book("1234", "Harry Potter"));
        lib.addBook(new Book("5678", "The boy in the striped pajamas"));
        lib.addBook(new Book("9012", "Think Java 6.1.3"));
        lib.addBook(new Book("3456", "Nu rekenen"));
        lib.addBook(new Book("7890", "Lord of the rings"));

        Book b1 = lib.getBook("9012");
        b1.returned();
        b1.loanToPerson();

        lib.addBook(new Book("2345", "Building a spaceship for dummies"));

        lib.getBook("9012").returned();

        Book b2 =  lib.getBook("8901");
        System.out.println(b2.getTitle());
    }

}
