package exercise.ch06;

public class Exercise_7 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(oddSum(13));
    }

    /**
     * Calculated the sum of all odd numbers from 1 to n
     * @param n The highest number
     * @return the sum of all odd numbers from 1 to n or 0 if n is even.
     */
    public static int oddSum(int n){
        if(n % 2 == 0 || n < 0) return 0; //Only process odd numbers
        return oddSum(n - 2) + n;
    }

}
