package exercise.ch05;

/**
 * 1. Draw a stack diagram that shows the state of the program the second time zoop() is invoked.
 *
 *      +--------------------------------------------+
 *      |         +-----------------------------+    |
 *      | main    | args [null]                 |    |
 *      |         +-----------------------------+    |
 *      +--------------------------------------------+
 *
 *      +--------------------------------------------+
 *      |         +-----------------------------+    |
 *      | clink   | fork [4]                    |    |
 *      |         +-----------------------------+    |
 *      +--------------------------------------------+
 *
 *      +--------------------------------------------+
 *      |         +-----------------------------+    |
 *      | zoop    | fred ["breakfast"]  bob [4] |    |
 *      |         +-----------------------------+    |
 *      +--------------------------------------------+
 *
 * 2. What is the complete output?
 *
 *      just for
 *      any not more
 *      It's breakfast
 *      !
 */

public class Exercise_2 {

    /**
     * Prints the contents of fred and depending on the value of bob prints 'not ' or '!'
     * @param fred A string that will always be printed
     * @param bob If 5, prints 'not ', otherwise prints '!'
     */
    public static void zoop(String fred, int bob){
        System.out.println(fred);
        if(bob == 5){
            ping("not ");
        } else {
            System.out.println("!");
        }
    }

    /**
     * Main mathod.
     * @param args
     */
    public static void main(String[] args) {
        int bizz = 5;
        int buzz = 2;
        zoop("just for", bizz);
        clink(2 * buzz);
    }

    /**
     * Prints "It's " and executes zoop
     * @param fork
     */
    public static void clink(int fork){
        System.out.print("It's ");
        zoop("breakfast ", fork);
    }

    /**
     * Prints a sentence with the provided parameter
     * @param strangStrung Will be printed. This can be anything.
     */
    public static void ping(String strangStrung){
        System.out.println("any " + strangStrung + "more ");
    }

}
