package exercise.ch06;

/**
 * Output of the program:
 *      true
 *      true
 *      ping!
 *      pong!
 */
public class Exercise_5 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        boolean flag1 = isHoopy(202);
        boolean flag2 = isFrabjuous(202);
        System.out.println(flag1);
        System.out.println(flag2);
        if(flag1 && flag2){
            System.out.println("ping!");
        }
        if(flag1 || flag2){
            System.out.println("pong!");
        }
    }

    /**
     * Checks if a number is hoopy by checking whether it is divisible by 2
     * @param x The number to compare
     * @return true if x is divisible by 2
     */
    public static boolean isHoopy(int x){
        boolean hoopyFlag;
        if(x % 2 == 0){
            hoopyFlag = true;
        } else {
            hoopyFlag = false;
        }
        return hoopyFlag;
        // This entire method could be rewritten with the following line of code
        //return x % 2 == 0;
    }

    /**
     * Checks if a number is frabjuous by checking if it is positive
     * @param x The number to check
     * @return true if x is positive
     */
    public static boolean isFrabjuous(int x){
        boolean frabjuousFlag;
        if(x > 0){
            frabjuousFlag = true;
        } else {
            frabjuousFlag = false;
        }
        return frabjuousFlag;
        // This entire method could be rewritten with the following line of code
        //return x > 0;
    }

}
