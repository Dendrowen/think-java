package exercise.ch11.exercise_2;

public class Time {

    private int seconds;
    private int minute;
    private int hour;

    /**
     * Constructor
     * @param hour
     * @param minute
     * @param seconds
     */
    public Time(int hour, int minute, int seconds){
        this.hour = hour;
        this.seconds = seconds;
        this.minute = minute;
    }

    /**
     * Increments the time with the given value
     * @param seconds Number of seconds to add to the current time
     */
    public void increment(int seconds){
        this.seconds += seconds;
        this.minute += this.seconds / 60;
        this.hour += this.minute / 60;

        this.minute %= 60;
        this.seconds %= 60;
    }

    /**
     * A String representation of the Time object
     * @return
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d",
                hour, minute, seconds);
    }
}
