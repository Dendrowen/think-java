package exercise.ch12.test;

import java.util.Random;

public class Dice {

    private static Random rnd = new Random();

    private final int sides;
    private int numberOnTop;

    public Dice(int sides){
        this.sides = sides;
        roll();
    }

    public int roll(){
        numberOnTop = rnd.nextInt(sides) + 1;
        return numberOnTop;
    }

    public int getNumberOnTop() {
        return numberOnTop;
    }

    public int getSides() {
        return sides;
    }

    public String toString(){
        return " [" + getNumberOnTop() + "] ";
    }
}
