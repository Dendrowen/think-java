package exercise.ch02;

public class Exercise_3 {

    final static int SECONDS_PER_DAY = 24 * 60 * 60;

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        // Step 2
        int hour = 15;
        int minute = 42;
        int second = 12;

        // Step 3
        int secondsSinceMidnight = hour * 60 * 60 + minute * 60 + second;

        System.out.print("Seconds since midnight:   ");
        System.out.println(secondsSinceMidnight);

        // Step 4
        int secondsUntilMidnight = SECONDS_PER_DAY - secondsSinceMidnight;

        System.out.print("Seconds until midnight:   ");
        System.out.println(secondsUntilMidnight);

        // Step 5
        int percentage = 100 * secondsSinceMidnight / SECONDS_PER_DAY;
        System.out.print("Percentage of day passed: ");
        System.out.print(percentage);
        System.out.println("%");

        // Step 6
        hour = 16;
        minute = 14;
        second = 3;

        System.out.println();
        System.out.print("Seconds passed since I started this exercise: ");
        System.out.println(hour * 60 * 60 + minute * 60 + second - secondsSinceMidnight);

    }

}
