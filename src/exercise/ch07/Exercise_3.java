package exercise.ch07;

public class Exercise_3 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(power(5, 3));
    }

    /**
     * Calculates the exponent (n) of x
     * @param x The base
     * @param n The exponent
     * @return x to the power n
     */
    public static double power(double x, int n){
        double result = x;
        while(n > 1){
            n--;
            result *= x;
        }
        return result;
    }

}
