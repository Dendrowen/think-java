package exercise.ch04;

public class Exercise_2 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        zool(11, "Sora", "Spanbroekerweg");
    }

    /**
     * An empty method
     * @param n     A number
     * @param s1    This will contain the name of my first pet
     * @param s2    This will contain the name of the street I grew up on
     */
    public static void zool(int n, String s1, String s2){ }

}
