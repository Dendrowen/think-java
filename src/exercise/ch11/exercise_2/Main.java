package exercise.ch11.exercise_2;

public class Main {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        Time time = new Time(12, 50, 50);
        System.out.println(time);
        time.increment(10000);
        System.out.println(time);

    }
}
