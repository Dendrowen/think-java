package exercise.crazy_eights;

public class Deck_old {

    private Card[] cards;

    public Deck_old(int n){
        this.cards = new Card[n];
    }

    public Deck_old(){
        cards = new Card[52];
        int index = 0;
        for(int suit = 0; suit <= 3; suit++){
            for(int rank = 1; rank <= 13; rank++){
                cards[index++] = new Card(rank, suit);
            }
        }
    }

    public void insertionSort(){
        for(int i = 1; i < cards.length; i++){
            Card t = cards[i];
            int j = i - 1;
            while(j >= 0 && t.compareTo(cards[j]) < 0){
                cards[j + 1] = cards[j];
                j--;
            }
            cards[j + 1] = t;
        }
    }

    public Deck_old mergeSort(){
        if(this.cards.length <= 1) return this;
        Deck_old d1 = subdeck(0, (cards.length - 1) / 2);
        Deck_old d2 = subdeck((cards.length - 1) / 2 + 1, cards.length - 1);
        d1 = d1.mergeSort();
        d2 = d2.mergeSort();
        return merge(d1, d2);
    }

    public void selectionSort(){
        for(int i = 0; i < cards.length; i++){
            swapCards(i, indexLowest(i, cards.length - 1));
        }
    }

    public int indexLowest(int low, int high){
        int l = low;
        while(low++ < high){
            if(cards[low].compareTo(cards[l]) < 0) l = low;
        }
        return l;
    }

    public Deck_old subdeck(int low, int high){
        Deck_old sub = new Deck_old(high - low + 1);
        for(int i = 0; i < sub.cards.length; i++){
            sub.cards[i] = this.cards[low + i];
        }
        return sub;
    }

    public void shuffle(){
        for(int i = 0; i < cards.length; i++){
            swapCards(i, randomInt(0, cards.length - 1));
        }
    }

    public void print(){
        for(int i = 0; i < cards.length; i++){
            System.out.println(cards[i]);
        }
    }

    public void swapCards(int a, int b){
        Card t = cards[a];
        cards[a] = cards[b];
        cards[b] = t;
    }

    public String toString(){
        String s = "";
        for(Card c : cards){
            s += c.toString() + "\n";
        }
        return s;
    }

    public static int randomInt(int low, int high){
        return (int) Math.floor(Math.random() * (high - low + 1)) + low;
    }

    public static Deck_old merge(Deck_old d1, Deck_old d2){
        Deck_old d = new Deck_old(d1.cards.length + d2.cards.length);
        int a = 0;
        int b = 0;
        for(int i = 0; i < d.cards.length; i++){
            if(a == d1.cards.length){
                d.cards[i] = d2.cards[b++];
            }
            else if(b == d2.cards.length){
                d.cards[i] = d1.cards[a++];
            }
            else if(d1.cards[a].compareTo(d2.cards[b]) < 0){
                d.cards[i] = d1.cards[a++];
            }
            else {
                d.cards[i] = d2.cards[b++];
            }
        }
        return d;
    }

}
