package exercise.ch08;

public class Exercise_4 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        int[] arr = {2, 6, 3, 10, 3};
        System.out.println(indexOfMax(arr));
    }

    /**
     * Takes an array and returns the index of the highest number
     * @param arr The array to examine
     * @return The index of the highest number in the array
     */
    public static int indexOfMax(int[] arr){
        int h = Integer.MIN_VALUE;
        int index = -1;
        for(int i = 0; i < arr.length; i++){
            if(arr[i] > h){
                h = arr[i];
                index = i;
            }
        }
        return index;
    }

}
