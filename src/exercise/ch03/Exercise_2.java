package exercise.ch03;

import java.util.Scanner;

public class Exercise_2 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Temperature in celsius: ");
        double temperatureInCelsius = in.nextDouble();

        double temperatureInFahrenheit = temperatureInCelsius * 9/5 + 32;

        System.out.printf("\n%.1f C = %.1f F\n", temperatureInCelsius, temperatureInFahrenheit);
    }

}
