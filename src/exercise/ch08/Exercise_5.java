package exercise.ch08;

public class Exercise_5 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        printTrues(sieve(3252));
    }

    /**
     * Print all indexes af which the value is true
     * @param arr The array
     */
    public static void printTrues(boolean[] arr){
        int c = 0;
        for(int i = 0; i < arr.length; i++){
            if(arr[i]){
                c++;
                System.out.printf("%8d", i);
                if(c == 20){
                    System.out.println();
                    c = 0;
                }
            }
        }
    }

    /**
     * calculate all primes up to max;
     * @param max The upper bound (excluded)
     * @return An array of booleans whether the index is a prime
     */
    public static boolean[] sieve(int max){
        boolean[] prime = new boolean[max];                     // create the array
        for(int i = 0; i < prime.length; i++) prime[i] = true;  // set all values to true

        int sieve = 2;
        while(sieve < max){
            for(int i = sieve * 2; i < max; i += sieve){
                prime[i] = false;
            }
            do {
                sieve++;
            } while (sieve < max && !prime[sieve]);
        }

        return prime;
    }

}
