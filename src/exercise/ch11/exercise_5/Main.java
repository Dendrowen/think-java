package exercise.ch11.exercise_5;

public class Main {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        Rational test = new Rational(3, 8);

        test.invert();

        Rational res = test.add(new Rational(1, 4));

        System.out.println(res);
    }

}
