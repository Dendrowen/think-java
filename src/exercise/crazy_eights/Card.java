package exercise.crazy_eights;

import java.util.Arrays;

public class Card {

    public static final String[] SUITS = {"Clubs", "Diamonds", "Hearts", "Spades"};
    public static final String[] RANKS = {null, "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};

    public static final int SUIT_CLUBS      = 0;
    public static final int SUIT_DIAMONDS   = 1;
    public static final int SUIT_HEARTS     = 2;
    public static final int SUIT_SPADES     = 3;

    public static final int RANK_ACE        = 1;
    public static final int RANK_2          = 2;
    public static final int RANK_3          = 3;
    public static final int RANK_4          = 4;
    public static final int RANK_5          = 5;
    public static final int RANK_6          = 6;
    public static final int RANK_7          = 7;
    public static final int RANK_8          = 8;
    public static final int RANK_9          = 9;
    public static final int RANK_10         = 10;
    public static final int RANK_JACK       = 11;
    public static final int RANK_QUEEN      = 12;
    public static final int RANK_KING       = 13;

    /**
     * Performs a sequential search
     * @param cards The deck of cards to search
     * @param target The card to find
     * @return The index of target or -1 if it doesn't exist
     */
    public static int search(Card[] cards, Card target){
        for(int i = 0; i < cards.length; i++){
            if(cards[i].equals(target)) return i;
        }
        return -1;
    }

    /**
     * Performs a binary search
     * @param cards The deck of cards to search
     * @param target The card to find
     * @return The index of target or -1 if it doesn't exist
     */
    public static int binarySearch(Card[] cards, Card target){
        int low = 0;
        int high = cards.length - 1;
        while(low <= high){
            int mid = (low + high) / 2;
            switch (cards[mid].compareTo(target)){
                case 0:
                    return mid;
                case -1:
                    low = mid + 1;
                    break;
                case 1:
                    high = mid - 1;
                    break;
            }

        }
        return -1;
    }

    /**
     * Chapter 12. Exercise 3. Step 1.
     * Returns a histogram for the suits of the cards
     * @param cards The cards to search through
     * @return A histogram of suits.
     */
    public static int[] suitHist(Card[] cards){
        int[] hist = new int[4];
        for(Card card : cards){
            hist[card.getSuit()]++;
        }
        return hist;
    }

    /**
     * Chapter 12. Exercise 3. Step 2.
     * Returns whether or not the provided set of cards contains 5 or more cards of the same suit.
     * @param cards The deck of cards
     * @return true if the provided cards contain 5 cards of the same suit
     */
    public static boolean hasFlush(Card[] cards){
        for(int n : suitHist(cards)){
            if(n >= 5) return true;
        }
        return false;
    }

    private final int rank;
    private final int suit;

    /**
     * Constructor
     * @param rank The rank of the card. e.g. 11 (Jack)
     * @param suit The suit of the card. e.g. 2 (Hearts)
     */
    public Card(int rank, int suit){
        this.rank = rank;
        this.suit = suit;
    }

    /**
     * Compares two cards and returns true if they are equal
     * @param that The card to compare this to
     * @return true if the suit and rank are equal
     */
    public boolean equals(Card that){
        return this.rank == that.rank &&
                this.suit == that.suit;
    }

    /**
     * Compares 2 cards to each other by value
     * @param that The card to compare this to
     * @return -1 if this is less, 1 if it is more. 0 if they are equal
     */
    public int compareTo(Card that){
        if(this.suit < that.suit) return -1;
        if(this.suit > that.suit) return 1;

        // Chapter 12. Exercise 2.
        if(this.rank != RANK_ACE && that.rank == RANK_ACE) return -1;
        if(this.rank == RANK_ACE && that.rank != RANK_ACE) return 1;

        if(this.rank < that.rank) return -1;
        if(this.rank > that.rank) return 1;
        return 0;
    }

    /**
     *
     * @return A string representation of this card
     */
    public String toString(){
        return RANKS[rank] + " of " + SUITS[suit];
    }

    // Getters
    public int getRank() {
        return rank;
    }

    public int getSuit() {
        return suit;
    }
}
