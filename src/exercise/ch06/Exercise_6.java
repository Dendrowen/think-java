package exercise.ch06;

/**
 * 1. Draw a stack diagram showing the state of the program just before the last invocation of prod completes
 *      +----------------------- ----------+
 *      |         +--------------+         |
 *      | main    | args [null]  |         |
 *      |         +--------------+         |
 *      +----------------------------------+
 *
 *      +----------------------------------+
 *      |         +--------------+         |
 *      | prod    | m [1]  n [4] |         |
 *      |         +--------------+         |
 *      +----------------------------------+
 *
 *      +----------------------------------+
 *      |         +--------------+         |
 *      | prod    | m [1]  n [3] |         |
 *      |         +--------------+         |
 *      +----------------------------------+
 *
 *      +----------------------------------+
 *      |         +--------------+         |
 *      | prod    | m [1]  n [2] |         |
 *      |         +--------------+         |
 *      +----------------------------------+
 *
 *      +----------------------------------+
 *      |         +--------------+         |
 *      | prod    | m [1]  n [1] |         |
 *      |         +--------------+         |
 *      +----------------------------------+
 *
 * 2. What is the output of this program?
 *      24 ( because 1 * 2 * 3 * 4 equals 24 )
 *
 * 3. Explain in a few words what prod does.
 *      See the method documentation
 *
 */

public class Exercise_6 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(prod(1, 4));
        System.out.println(prodImproved(1, 4));
    }

    /**
     * Calculates the product of all numbers from m to n
     * @param m lower number. Should be smaller or equal to n
     * @param n Higher number. Should be greater or equal to m
     * @return The product of all numbers from m to n
     */
    public static int prod(int m, int n) {
        if(m == n){
            return n;
        } else {
            int recurse = prod(m, n - 1);
            int result = n * recurse;
            return result;
        }
    }

    /**
     * Calculates the product of all numbers from m to n
     * @param m lower number. Should be smaller or equal to n
     * @param n Higher number. Should be greater or equal to m
     * @return The product of all numbers from m to n
     */
    public static int prodImproved(int m, int n){
        if(m == n) return n;
        return prod(m, n - 1) * n;
    }

}
