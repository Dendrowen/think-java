package exercise.ch09;

public class Exercise_4 {

    public static void main(String[] args) {
        System.out.println(isPalindrome("testing"));
        System.out.println(isPalindrome("eibofobie"));
    }

    public static boolean isPalindrome(String s){ // Step 5
        while(true){
            char first = first(s);
            char last = first(reverseString(s));
            if(first != last){
                return false;
            }
            s = middle(s);
            if (length(s) < 2) break;
        }
        return true;
    }

    public static String reverseString(String s){ // Step 4
        if(length(s) == 0) return "";
        return reverseString(rest(s)) + first(s);
    }

    public static void printBackwards(String s){ // Step 3
        if(length(s) == 0) return;
        printBackwards(rest(s));
        System.out.println(first(s));
    }

    public static void printString(String s){ // Step 2
        if(length(s) == 0) return;
        System.out.println(first(s));
        printString(rest(s));
    }

    public static char first(String s){
        return s.charAt(0);
    }

    public static String rest(String s){
        return s.substring(1);
    }

    public static String middle(String s){
        return s.substring(1, s.length() - 1);
    }

    public static int length(String s){
        return s.length();
    }
}
