package exercise.crazy_eights;

public class Player {

    private String name;
    private Hand hand;

    public Player(String name){
        this.name = name;
        this.hand = new Hand(name);
    }

    public Hand getHand() {
        return hand;
    }

    public String getName() {
        return name;
    }

    public void displey(){
        System.out.println("Player: " + name);
        hand.display();
    }

    public Card play(Eights eights, Card prev){
        Card card = searchForMatch(prev);
            if(card == null){
                card = drawForMatch(eights, prev);
            }
        return card;
    }

    public Card searchForMatch(Card prev){
        for(int i = 0; i < hand.size(); i++){
            if (cardMatches(hand.getCard(i), prev)){
                return hand.popCard(i);
            }
        }
        return null;
    }

    public Card drawForMatch(Eights eights, Card prev){
        while(true){
            Card c = eights.draw();
            System.out.println(name + " draws " + c);
            if(cardMatches(c, prev)){
                return c;
            }
            hand.addCard(c);
        }
    }

    public int score(){
        int sum = 0;
        for(int i = 0; i < hand.size(); i++){
            Card c = hand.getCard(i);
            if(c.getRank() == 8){
                sum -= 20;
            } else if (c.getRank() > 10){
                sum -= 10;
            } else {
                sum -= c.getRank();
            }
        }
        return sum;
    }

    public static boolean cardMatches(Card c1, Card c2){
        if(c1.getSuit() == c2.getSuit()){
            return true;
        }
        if(c1.getRank() == c2.getRank()){
            return true;
        }
        if(c1.getRank() == 8){
            return true;
        }
        return false;
    }
}
