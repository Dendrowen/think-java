package exercise.ch06;

public class Exercise_8 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(ack(3, 5));
    }

    /**
     * Calculates the Ackermann mathematical function
     * @param m
     * @param n
     * @return The result of the Ackermann calculation using m and n
     */
    public static int ack(int m, int n){
        if(m == 0) return n + 1;
        if(m > 0 && n == 0) return ack(m - 1, 1);
        if(m > 0 && n > 0) return ack(m - 1, ack(m, n - 1));
        return 0;
    }

}
