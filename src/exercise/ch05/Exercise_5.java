package exercise.ch05;

public class Exercise_5 {

    public static final int DELAY_BETWEEN_LINES = 2000; // in milliseconds
    public static final int NUMBER_OF_VERSES    = 100; // including 0

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        try {
            bottles(NUMBER_OF_VERSES - 1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Executes verses of the 99 bottles song until there are no bottles left
     * @param bottles The number of bottles remaining
     * @throws InterruptedException
     */
    public static void bottles(int bottles) throws InterruptedException {
        System.out.println(convert(bottles).replace('n', 'N') + " bottles of beer on the wall,");
        Thread.sleep(DELAY_BETWEEN_LINES);
        System.out.println(convert(bottles) + " bottles of beer,");
        Thread.sleep(DELAY_BETWEEN_LINES);
        thirdLine(bottles);
        Thread.sleep(DELAY_BETWEEN_LINES);
        forthLine(bottles);
        Thread.sleep(DELAY_BETWEEN_LINES);
        System.out.println();
        if(bottles > 0) {
            bottles(bottles - 1);
        }
    }

    /**
     * Converts 0 to 'no'. Otherwise returns the number as a String
     * @param bottles The number to convert
     * @return
     */
    public static String convert(int bottles){
        if(bottles == 0){
            return "no";
        } else {
            return String.valueOf(bottles);
        }
    }

    /**
     * Prints the third line of the verse
     * @param bottles The number of bottles
     */
    public static void thirdLine(int bottles){
        if(bottles > 0){
            System.out.println("ya' take one down, ya' pass it around,");
        } else {
            System.out.println("ya' can't take one down, ya' can't pass it around,");
        }
    }

    /**
     * Prints the forth line of the verse
     * @param bottles The number of bottles
     */
    public static void forthLine(int bottles){
        if(bottles > 0){
            System.out.println(convert(bottles-1) + " bottles of beer on the wall.");
        } else {
            System.out.println("'cause there are no more bottles of beer on the wall!");
        }
    }

}
