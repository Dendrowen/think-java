package exercise.ch08;

public class Exercise_2 {

    /**
     * Multiplies all values in the array and returns the product
     * @param a The array to multiply
     * @return The product of all values
     */
    public static int banana(int[] a){
        int kiwi = 1;
        int i = 0;
        while(i < a.length){
            kiwi = kiwi * a[i];
            i++;
        }
        return kiwi;
    }

    /**
     * Searches the provided array for the value 'grape' and returns the index of -1 if it isn't found
     * @param a The array
     * @param grape The search query
     * @return The index of 'grape' or -1 if it isn't found
     */
    public static int grapefruit(int[] a, int grape){
        for(int i = 0; i < a.length; i++){
            if(a[i] == grape){
                return i;
            }
        }
        return -1;
    }

    /**
     * Counts the number of values 'apple' in the array 'a'
     * @param a The array
     * @param apple The search query
     * @return The number of found values that match 'apple'
     */
    public static int pineapple(int[] a, int apple){
        int pear = 0;
        for(int pine : a){
            if(pine == apple){
                pear++;
            }
        }
        return pear;
    }

}
