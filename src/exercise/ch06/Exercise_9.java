package exercise.ch06;

public class Exercise_9 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        System.out.printf("%.0f\n", power(5, 16));
        System.out.printf("%.0f\n", powerImproved(5, 16));
    }

    /**
     * Calculates the exponent (n) of x
     * @param x the base number
     * @param n the exponent
     * @return x to the power n
     */
    public static double power(double x, int n){
        System.out.println(n);
        if(n == 1) return x;
        return x * power(x, n - 1);
    }

    /**
     * Calculates the exponent (n) of x. Although this method seems to be more work, it is actually less processor intensive.
     * @param x the base number
     * @param n the exponent
     * @return x to the power n
     */
    public static double powerImproved(double x, int n){
        System.out.println(n);
        if(n == 1) return x;
        if(n % 2 == 0 && n > 2) return powerImproved(powerImproved(x, n / 2), 2);
        return x * powerImproved(x, n - 1);
    }

}
