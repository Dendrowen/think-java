package exercise.ch05;

/**
 * 1 & 2. Write numbers next to the lines of code in order of execution
 *      See the class file
 *
 * 3. What is the value of the parameter 'blimp' when baffle gets invoked?
 *      rattle
 *
 * 4. What is the output of this program?
 *      ik
 *      rattle
 *      ping zoop
 *      boo-wa-ha-ha
 *
 */

public class Exercise_6 {

    /**
     * Prints the parameter and executes zippo()
     * @param blimp the value to be printed
     */
    public static void baffle(String blimp){
        System.out.println(blimp);                          // 4
        zippo("ping", -5);                                  // 5
    }

    /**
     * Depending on the flag, prints a certain text
     * @param quince The text to be printed
     * @param flag The flag determining the other output
     */
    public static void zippo(String quince, int flag){
        if(flag < 0){
            System.out.println(quince + " zoop");           // 6
        } else {
            System.out.println("ik");                       // 2
            baffle(quince);                                 // 3
            System.out.println("boo-wa-ha-ha");             // 7
        }
    }

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        zippo("rattle", 13);                                // 1
    }

}
