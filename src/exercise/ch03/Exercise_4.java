package exercise.ch03;

import java.util.Random;
import java.util.Scanner;

public class Exercise_4 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random r = new Random();


        int number = r.nextInt(100) + 1;

        System.out.println("I'm thinking of a number between 1 and 100");
        System.out.println("(Including both). Can you guess what it is?");
        System.out.print  ("Type a number: ");

        int guess = in.nextInt();

        System.out.printf ("Your guess is: %d\n", guess);
        System.out.printf ("The number I was thinking of is: %d\n", number);
        System.out.printf ("You were off by: %d\n", Math.abs(guess - number));
    }

}
