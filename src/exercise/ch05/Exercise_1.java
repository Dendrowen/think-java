package exercise.ch05;

import java.util.Scanner;

public class Exercise_1 {

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("enter a number: ");
        int x = in.nextInt();

        // =================== ORIGINAL CODE ====================
        if(x > 0){
            if(x < 10){
                System.out.println("Positive single digit number");
            }
        }

        // =================== SIMPLIFIED CODE ====================
        if(x > 0 && x < 10){
            System.out.println("Positive single digit number");
        }
    }

}
