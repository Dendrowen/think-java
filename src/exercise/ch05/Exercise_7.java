package exercise.ch05;

import java.util.Random;
import java.util.Scanner;

public class Exercise_7 {

    //Raise this number to make the game more difficult
    public static final int MAX_NUMBER = 100;

    public static int numberToGuess;
    public static int tries;
    public static Scanner in;

    /**
     * Main method.
     * @param args
     */
    public static void main(String[] args) {
        in = new Scanner(System.in);
        generateNumber(MAX_NUMBER);
        guessNumber();
    }

    /**
     * Generates a random number from 1 to the limit and stores it
     * @param limit The upper limit. included
     */
    public static void generateNumber(int limit){
        Random r = new Random();
        numberToGuess = r.nextInt(limit) + 1;
        tries = 0;
    }

    /**
     * Let's the user guess a number, returns if the user should
     * guess higher or lower and repeats itself until the number
     * is guessed
     */
    public static void guessNumber(){
        tries++;
        int guess;

        do {
            System.out.print("Guess a number: ");
            guess = in.nextInt();
        } while (guess < 1 || guess > MAX_NUMBER);

        if(guess == numberToGuess){
            System.out.printf("Congratulations! You needed %d tries\n", tries);
        } else {
            System.out.println(guess < numberToGuess ? "higher" : "lower");
            guessNumber();
        }

    }

}
