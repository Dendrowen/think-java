package exercise.ch13.test;

public class Library {

    private final String name;
    private Book[] books;

    public Library(String name, int numberOfBooks){
        this.name = name;
        this.books = new Book[numberOfBooks];
    }

    public void addBook(Book book){
        int i = 0;
        while(books[i] != null){
            i++;
            if(i == books.length){
                System.out.println("ERROR: library is full!");
                return;
            }
        }
        books[i] = book;
    }

    public Book getBook(String isbn){
        for(int i = 0; i < books.length; i++){
            if(books[i].getIsbn().equals(isbn)){
                return books[i];
            }
        }
        return null;
    }

}
