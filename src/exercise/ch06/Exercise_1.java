package exercise.ch06;

public class Exercise_1 {

    /**
     * 1. What happens if you invoke a value method and don't do anything with the result?
     *      Nothing. The result gets ignored
     *
     * 2. What happens if you use a void method as part of an expression?
     *      Your IDE won't compile your code.
     */

}
